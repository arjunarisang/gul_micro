package com.gul.micro.product.dao;

import com.gul.micro.product.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ProductRepo extends CrudRepository<Product, Long> {
}
