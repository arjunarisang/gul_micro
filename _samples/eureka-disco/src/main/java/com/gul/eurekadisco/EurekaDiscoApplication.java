package com.gul.eurekadisco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaDiscoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaDiscoApplication.class, args);
	}
}
