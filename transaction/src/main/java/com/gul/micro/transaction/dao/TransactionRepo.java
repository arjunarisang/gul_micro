package com.gul.micro.transaction.dao;

import com.gul.micro.transaction.entity.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TransactionRepo extends CrudRepository<Transaction, Long> {
}
