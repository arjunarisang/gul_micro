# Latihan microservice

Sumber:
* https://www.thinglink.com/scene/1001820105673277443
* http://pscode.rs/spring-cloud-remote-config/
* https://store.docker.com/images/vault (lihat di section yang ada `dumb-init`)

## Setup Docker

```
$ docker run -d -p 8200:8200 -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200' --name vaultku vault
```

Lalu masuk ke container.

```
$ docker exec -it <container_name/id> sh
```

Di vault shell.

```
/ export VAULT_ADDR='http://0.0.0.0:8200'
/ vault auth myroot
```

`myroot` di atas adalah token root yang kita setup ketika menjalankan `docker run`. **Ingat!** Hanya boleh digunakan di developemnt.

## Configure Vault untuk externalized configuration

### productservice

`bootstrap.yml`

```
vault write secret/productservice server.port=8001
```