package com.gul.micro.user.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String nama;

    private String email;

    public User() {
    }

    public User(Long id, String nama, String email) {
        this.id = id;
        this.nama = nama;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
