package com.gul.micro.user.dao;

import com.gul.micro.user.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserRepo extends CrudRepository<User, Long> {
}
